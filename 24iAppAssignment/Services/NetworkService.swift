//
//  NetworkService.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit
import Network

class NetworkService {

    static let shared = NetworkService()
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "InternetConnectionMonitor")

    func checkInternet( result:@escaping (Bool) -> Void)  {
        var hasConnection = Bool()
        monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                print("Internet connection is on.")
                hasConnection = true
            } else {
                print("There's no internet connection.")
                hasConnection = false
            }
            result(hasConnection)
        }
        monitor.start(queue: queue)
    }
}
