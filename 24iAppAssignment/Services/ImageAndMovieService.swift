//
//  imageAndMovieService.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 13/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit

class ImageAndMovieService {

   static func setUrlForImage(movie: Movie, completion:@escaping (URL) -> Void) {
        guard let posterPath = movie.posterPath else {return}
        let path = "https://image.tmdb.org/t/p/w500/" + posterPath
        guard let url = URL(string: path) else {return}
        completion(url)
    }

    static func setUrlForTrailer(urlPath: String, completion:@escaping (URL) -> Void) {
        let urlString = "https://www.youtube.com/embed/\(urlPath)"
        guard let url = URL(string: urlString) else {return}
        completion(url)
    }
}
