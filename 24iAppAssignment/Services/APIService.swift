//
//  APIService.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit
import TMDBSwift

class APIService {

   static let ApiKey = "f495d3a974d6bcc9708a3ab8c5238042"

    // Create alerts in following funcs for error and data if error is not nil and data is
    static func getPopularMovies(completion: @escaping (MovieList) -> Void) {
        let url = "https://api.themoviedb.org/3/movie/popular?api_key=\(ApiKey)&language=en-US&page=1"
        let task = URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) in
            assert(error == nil)
            guard let data = data else { return }
            let decoder = JSONDecoder()
            let list = try! decoder.decode(MovieList.self, from: data)
            completion(list)
        }
        task.resume()
    }

    static func getGenre(movie: Movie, completion: @escaping (Genres) -> Void){
        guard let movieID = movie.id else {return}
        let url = "https://api.themoviedb.org/3/movie/\(movieID)?api_key=\(ApiKey)&language=en-US"
        let task = URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) in
            assert(error == nil)
            guard let data = data else { return }
            let decoder = JSONDecoder()
            let list = try! decoder.decode(Genres.self, from: data)
            completion(list)
        }
        task.resume()
    }

    static func getVideo(movie: Movie, completion: @escaping (videoID) -> Void) {
        guard let movieID = movie.id else {return}
        let url = "https://api.themoviedb.org/3/movie/\(movieID)/videos?api_key=\(ApiKey)&language=en-US"
        let task = URLSession.shared.dataTask(with: URL(string: url)!) { (data, response, error) in
            assert(error == nil)
            guard let data = data else { return }
            let decoder = JSONDecoder()
            let list = try! decoder.decode(videoID.self, from: data)
            completion(list)
        }
        task.resume()
    }
}
