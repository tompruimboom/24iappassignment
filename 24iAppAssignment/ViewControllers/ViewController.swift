//
//  ViewController.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!

    var movieList: MovieList?
    var movie: Movie?
    var selectedIndex = Int()
    var searchActive = false
    var movieTitlesArray = [Movie]()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        deselectRow(animated: true)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        APIService.getPopularMovies { (list) in
            DispatchQueue.main.async {
                self.movieList = list
                self.tableView.reloadData()
            }
        }

        setup()
    }

    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        searchBar.showsCancelButton = true

        navigationItem.hidesBackButton = true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive {
            return movieTitlesArray.count
        }
        guard let count = movieList?.results.count else {return 0}
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath)
        guard let movieCell = cell as? MovieTableViewCell else {
            preconditionFailure()
        }

        // if case of searching
        if searchActive {
            let movie = movieTitlesArray[indexPath.item]
            movieCell.titleLabel.text = movie.title

            ImageAndMovieService.setUrlForImage(movie: movie) { (url) in
                movieCell.movieImageView.kf.setImage(with: url)
            }
            return movieCell
        }
        // In case of no search
        guard let movie = movieList?.results[indexPath.item] else {return cell}
        movieCell.titleLabel.text = movie.title

        ImageAndMovieService.setUrlForImage(movie: movie) { (url) in
            movieCell.movieImageView.kf.setImage(with: url)
        }
        return movieCell
    }

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // if searching
        if searchActive {
            let selectedIndex = indexPath.item
            movie = movieTitlesArray[selectedIndex]
            performSegue(withIdentifier: "detailSegue", sender: self)
        } else {
            let selectedIndex = indexPath.item
            movie = movieList?.results[selectedIndex]
            performSegue(withIdentifier: "detailSegue", sender: self)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue" {
            let controller = segue.destination as? DetailViewController
            controller?.movie = self.movie
        }
    }

    func deselectRow(animated: Bool) {
        if let indexPathForSelectedRow = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathForSelectedRow, animated: animated)
        }
    }

    // searchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text != "" {
            searchActive = true
        }
    }

    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
        searchBar.resignFirstResponder()
        searchBar.text = ""
        tableView.reloadData()
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchActive = true
        guard let results = movieList?.results else { return }
        movieTitlesArray = results.compactMap { movie in
            guard let movieTitle = movie.title else { return nil }
            return movieTitle.contains(searchText) ? movie : nil
        }
        if searchText == "" {
            searchActive = false
            tableView.reloadData()
            return
        }
        tableView.reloadData()
    }
}

