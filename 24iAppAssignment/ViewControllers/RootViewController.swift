//
//  RootViewController.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit
import Network


class RootViewController: UIViewController {

    @IBOutlet weak var tryAgainButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup(){
        tryAgainButton.isHidden = true
        internetCheck()
    }

    @IBAction func tryAgainButtonTapped(_ sender: UIButton) {
        internetCheck()
    }

    func internetCheck() {
        NetworkService.shared.checkInternet() { (bool) in
            DispatchQueue.main.async {
                if bool {
                    self.performSegue(withIdentifier: "openAppSegue", sender: self)
                }
                if !bool {
                    let alertController = UIAlertController(title: NSLocalizedString("No internet", comment: ""), message: NSLocalizedString("This iPhone seems to have no connection with the internet.", comment: ""), preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel) { (action) in
                        self.tryAgainButton.isHidden = false
                    }
                    let openSettingsAction = UIAlertAction(title: NSLocalizedString("Open settings", comment: ""), style: .default) { (action) in
                        self.tryAgainButton.isHidden = false
                        guard let settingsUrl = URL(string: "App-Prefs:root=MOBILE_DATA_SETTINGS_ID") else { return  }
                    if UIApplication.shared.canOpenURL(settingsUrl) {
                        UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            print("Opened settings was success")
                            })
                        }
                    }
                    alertController.addAction(openSettingsAction)
                    alertController.addAction(alertAction)
                    self.present(alertController, animated: true)
                }
            }
        }
    }
}
