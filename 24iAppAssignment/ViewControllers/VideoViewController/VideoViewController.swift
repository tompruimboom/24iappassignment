//
//  VideoViewController.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import WebKit
import UIKit

class VideoViewController: UIViewController, WKNavigationDelegate {

    // Make automatically go back to previous viewController after finishing trailer. 

    var videoUrl: URL?
    @IBOutlet weak var videoView: WKWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var returnButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()

        guard let url = videoUrl else {return}
        let videoID = url.absoluteString
        let embededHTML = "<html><body style='margin:0px;padding:0px;'><script type='text/javascript' src='http://www.youtube.com/iframe_api'></script><script type='text/javascript'>function onYouTubeIframeAPIReady(){ytplayer=new YT.Player('playerId',{events:{onReady:onPlayerReady}})}function onPlayerReady(a){a.target.playVideo();}</script><iframe id='playerId' type='text/html' width='0' height='0' src='\(videoID)?enablejsapi=1&rel=0&playsinline=1&autoplay=1' frameborder='0'></body></html>"

        videoView.loadHTMLString(embededHTML, baseURL: nil)
    }

    func setup () {
        videoView.navigationDelegate = self
        videoView.configuration.mediaTypesRequiringUserActionForPlayback = []
        activityIndicator.startAnimating()
        activityIndicator.hidesWhenStopped = true
    }

    @IBAction func returnButtonPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        if webView == videoView {
            activityIndicator.stopAnimating()
        }
    }
}
