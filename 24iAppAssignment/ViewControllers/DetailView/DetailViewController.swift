//
//  DetailViewController.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var overViewLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mainStack: UIStackView!

    var movie: Movie?
    var videoUrl: URL?
    var genres = Genres()

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        titleLabel.text = movie?.title
        dateLabel.text = movie?.releaseDate
        overViewLabel.text = movie?.overview
        scrollView.isScrollEnabled = true

        guard let pickedMovie = movie else {return}
        ImageAndMovieService.setUrlForImage(movie: pickedMovie) { (url) in
            self.movieImageView.kf.setImage(with: url)
        }
        
        guard let selectedMovie = movie else {return}
        APIService.getGenre(movie: selectedMovie) { (genres) in
            DispatchQueue.main.async {
                let genreInfo = genres.genres?.map({ (genresInfo) -> String in
                    return (genresInfo.name ?? "")
                })
                let genryTypes = genreInfo?.joined(separator: ", ")
                self.genresLabel.text = genryTypes
            }
        }
    }

    @IBAction func watchTrailerButtonPressed(_ sender: Any) {
        guard let movieVideo = self.movie else {return}

        APIService.getVideo(movie: movieVideo) { (video) in
            DispatchQueue.main.async {
                guard let videoToPlay = video.results?.first else {return}
                guard let urlPath = videoToPlay.key else {return}

                ImageAndMovieService.setUrlForTrailer(urlPath: urlPath) { (url) in
                    self.videoUrl = url
                    self.performSegue(withIdentifier: "showVideoSegue", sender: self)
                }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showVideoSegue" {
            let controller = segue.destination as? VideoViewController
            controller?.videoUrl = self.videoUrl
        }
    }
}
