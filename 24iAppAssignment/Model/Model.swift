//
//  Movie.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit

struct MovieList: Codable {
    let results: [Movie]
}

struct Movie: Codable {

    var title: String?
    var releaseDate: String?
    var posterPath: String?
    var overview: String?
    var genres: [Int]?
    var id: Int?

    enum CodingKeys: String, CodingKey {
        case title = "original_title"
        case releaseDate = "release_date"
        case posterPath = "poster_path"
        case overview = "overview"
        case genres = "genre_ids"
        case id = "id"
    }
}

struct Genres: Codable {
    var genres: [GenresInfo]?
}

struct GenresInfo: Codable {
    var id: Int?
    var name: String?
}

struct videoID: Codable {
    var results: [video]?
}

struct video: Codable {
    var id: String?
    var key: String?
}

