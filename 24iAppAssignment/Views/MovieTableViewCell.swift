//
//  MovieTableViewCell.swift
//  24iAppAssignment
//
//  Created by Tom Pruimboom on 11/11/2019.
//  Copyright © 2019 Tom Pruimboom. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
}
